package com.qanalytics.api.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Teodor
 */
public class TestCase {

    private final String name;
    private List<String> steps;

    public TestCase( String name) {
        this.name = name;
        steps = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<String> getSteps() {
        return steps;
    }

    public void setSteps(List<String> steps) {
        this.steps = steps;
    }

    public void addStep(String step){
        this.steps.add(step);
    }
}
