package com.qanalytics.api.controller;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.qanalytics.api.Settings;
import com.qanalytics.api.model.Greeting;
import com.qanalytics.api.model.TestCase;
import com.qanalytics.api.utils.FileUtils;
import com.qanalytics.api.utils.GitUtils;
import com.qanalytics.api.utils.JsonUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Teodor
 */
@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private static final String REMOTE_URL = "https://github.com/github/testrepo.git";


    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

    @RequestMapping("/clone")
    public void closeRepository(@RequestParam(value = "url", defaultValue=REMOTE_URL) String url)throws IOException, InvalidRemoteException, TransportException, GitAPIException {
        File localPath = File.createTempFile("TestGitRepository", "");
        localPath.delete();

        System.out.println("Cloning from " + url + " to " + localPath);
        try (Git result = Git.cloneRepository()
                .setURI(url)
                .setDirectory(localPath)
                .call()) {
            // Note: the call() returns an opened repository already which needs to be closed to avoid file handle leaks!
            System.out.println("Having repository: " + result.getRepository().getDirectory());
        }
    }

    @RequestMapping("/cloneAndAnalyse")
    public String closeRepositoryAndAnalyse(@RequestParam(value = "url", defaultValue=REMOTE_URL) String url, @RequestParam(value = "filetype", defaultValue="story") String fileType ) throws IOException, GitAPIException {
        String path = GitUtils.cloneRepositoryIntoTempFolder(url);

        List<String> pathList = new ArrayList<>();
        FileUtils.findFile(fileType,new File(path), pathList);

        List<TestCase> testCases = FileUtils.generateTestCasesList(pathList);
        String json = JsonUtils.createChartDataInJson(testCases);
        System.out.println(json);
        return json;
    }



    @RequestMapping("/fetch")
    public String fetchDataFromGit(@RequestParam(value="name", defaultValue="World") String name) throws IOException {
        URL url = new URL(Settings._GIT_PATH);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();

        JsonParser jp = new JsonParser(); //from gson
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
        JsonObject rootobj = root.getAsJsonObject(); //May be an array, may be an object.
        //zipcode = rootobj.get("zip_code").getAsString(); //just grab the zipcode

        return rootobj.toString();
    }

    @RequestMapping("/fetch/story")
    public String fetchStoryFromGit(@RequestParam(value="name", defaultValue="World") String name) throws IOException {
        URL url = new URL(Settings._GIT_STORY_PATH);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();

        // create the albums object
        JsonObject story = new JsonObject();
        // add a property calle title to the albums object
        story.addProperty("title", "somestory");

        // create an array called datasets
        JsonArray steps = new JsonArray(), metas = new JsonArray();
        // create a dataset
        JsonObject step = new JsonObject(), meta = new JsonObject();


        StringBuffer text = new StringBuffer();
        InputStreamReader in = new InputStreamReader((InputStream) request.getContent());
        BufferedReader buff = new BufferedReader(in);
        String line;
        int counter = 0;
        do {
            line = buff.readLine();
            if(!(line == null)) {
                if (line.startsWith("Given") || line.startsWith("When") || line.startsWith("Then")) {
                    step.addProperty("step_" + counter++, line);
                } else {
                    meta.addProperty("meta_" + counter++, line);
                }
            }
            text.append(line + "\n");
        } while (line != null);

        steps.add(step);
        metas.add(meta);

        story.add("metas", metas);
        story.add("steps", steps);

        // create the gson using the GsonBuilder. Set pretty printing on. Allow
        // serializing null and set all fields to the Upper Camel Case
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
        System.out.println(gson.toJson(story));



        return gson.toJson(story);
    }

    @RequestMapping("/fetch/stories")
    public String fetchStories(@RequestParam(value="name", defaultValue="World") String name) throws IOException {
        URL url = new URL(Settings._GIT_PATH);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();

        JsonParser jp = new JsonParser(); //from gson
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
        JsonObject rootobj = root.getAsJsonObject(); //May be an array, may be an object.

        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

        List<String> list = new ArrayList<String>();
        list = gson.fromJson(rootobj.get("files"), new TypeToken<ArrayList<ArrayList<String>>>() {}.getType());


        //zipcode = rootobj.get("zip_code").getAsString(); //just grab the zipcode

        return list.toString();
    }
}
