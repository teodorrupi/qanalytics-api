package com.qanalytics.api;

/**
 * @author Teodor
 */
public interface Settings {

    String _GIT_PATH = "https://api.bitbucket.org/1.0/repositories/teodorrupi/qanalytics-api/src/master/";
    String _GIT_STORY_PATH = "https://api.bitbucket.org/1.0/repositories/teodorrupi/qanalytics-api/raw/master/src/test/test/test_story.story";
}
