package com.qanalytics.api.utils;

import com.google.gson.*;
import com.qanalytics.api.model.TestCase;

import java.util.List;

/**
 * @author Teodor
 */
public class JsonUtils {

    public static String createChartDataInJson(List<TestCase> testCaseList){
        //main json object
        JsonObject main = new JsonObject();
        JsonArray datas = new JsonArray();

        //creating classes array
        JsonObject _classes = new JsonObject();
        JsonArray _class = new JsonArray();
        _class.add("");
        _class.add("md-yellow-200-bg");

        int _storyCounter = 0;
        String _constantDate = "T00:00:00.000Z";

        for(TestCase tk : testCaseList){
            JsonObject data = new JsonObject();
            data.addProperty("id", "id1");
            data.addProperty("name", tk.getName());

            JsonArray tasks = new JsonArray();
            for(int x=0; x<tk.getSteps().size(); x++){
                JsonObject task = new JsonObject();
                task.addProperty("id", "id" + "_" + _storyCounter + "_" + x);
                task.addProperty("name", tk.getSteps().get(x));
                task.add("classes", _class);

                int i = x;
                i = i%7;
                String _year = i>7 ? "2017" : "2016";
                int _month_from_int = (int)(i+i*0.5)+1;
                String _month_from = _month_from_int<10 ? "0"+ String.valueOf(_month_from_int) : String.valueOf(_month_from_int);
                int _month_to_int = (int)(i+i*0.5)+2;
                String _month_to = _month_to_int<10 ? "0"+ String.valueOf(_month_to_int) : String.valueOf(_month_to_int);
                String date_from_to = (((i+i*0.5)+1)/0.5)%2 > 0 ? "15" : "01";
                task.addProperty("from", _year + "-" + _month_from + "-" + date_from_to + _constantDate);
                task.addProperty("to", _year + "-" + _month_to + "-" + date_from_to + _constantDate);
                task.addProperty("progress", 5);

                JsonArray dependencies = new JsonArray();
                JsonObject _dependency= new JsonObject(), _otherDependencyInForLoop = new JsonObject();
                _dependency.addProperty("to", "sommeDependancy");
//                _otherDependencyInForLoop.addProperty("to", "sommeDependancy");
                dependencies.add(_dependency);
//                dependencies.add(_otherDependencyInForLoop);
//                _tasks.add("dependencies", dependencies);

                task.add("dependencies", dependencies);

                tasks.add(task);
            }

            _storyCounter++;

            data.add("tasks", tasks);
            data.addProperty("data", "some text here");
            datas.add(data);
        }

        main.add("data", datas);


        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
        return gson.toJson(main);
    }
}
