package com.qanalytics.api.utils;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;

import java.io.File;
import java.io.IOException;

/**
 * @author Teodor
 */
public class GitUtils {

    /**
     * Clones a git repository that is public and returns the local path of clone folder
     *
     * @param gitUrl url of git repo to clone
     * @return Repository path cloned locally
     * @throws IOException
     * @throws InvalidRemoteException
     * @throws TransportException
     * @throws GitAPIException
     */
    public static String cloneRepositoryIntoTempFolder(String gitUrl) throws IOException, InvalidRemoteException, TransportException, GitAPIException {
        File localPath = File.createTempFile("TestGitRepository", "");
        localPath.delete();

        System.out.println("Cloning from " + gitUrl + " to " + localPath);
        try (Git result = Git.cloneRepository()
                .setURI(gitUrl)
                .setDirectory(localPath)
                .call()) {
            // Note: the call() returns an opened repository already which needs to be closed to avoid file handle leaks!
            System.out.println("Having repository: " + result.getRepository().getDirectory());
        }
        return localPath + "";
    }
}
