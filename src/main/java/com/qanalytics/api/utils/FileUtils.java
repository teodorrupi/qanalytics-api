package com.qanalytics.api.utils;

import com.qanalytics.api.model.TestCase;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Teodor
 */
public class FileUtils {


    /**
     * Does a recursive search starting on a File(Dir) to find files with extension
     * name and adds the absolute file paths to filePaths list
     *
     * @param name file extension
     * @param file File to search recursively
     * @param filePaths Array where paths are added
     */
    public static void findFile(String name,File file, List<String> filePaths)
    {
        File[] list = file.listFiles();
        if(list!=null)
            for (File fil : list)
            {
                if(fil.getName().contains(".git")){
                }
                else if (fil.isDirectory()) {
                    findFile(name, fil, filePaths);
                }
                else if (name.equalsIgnoreCase(fil.getName().split("\\.")[fil.getName().split("\\.").length - 1])) {
                    System.out.println("Found at" + fil.getAbsolutePath());
                    filePaths.add(fil.getAbsolutePath());
                }
            }
    }

    public static List<TestCase> generateTestCasesList(List<String> paths) throws IOException {
        List<TestCase> testCases = new ArrayList<>();
        for(String path: paths){
            FileInputStream fis = new FileInputStream(new File(path));

            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));

            String line = null;
            TestCase testCase = null;
            while ((line = br.readLine()) != null) {
                if(line.startsWith("Scenario")){
                    testCase = new TestCase(line);
                    testCases.add(testCase);
                }
                else if(line.startsWith("Given") || line.startsWith("When") || line.startsWith("And") || line.startsWith("Then")){
                    if(testCase!=null){
                        testCase.addStep(line);
                    }
                }
            }

            br.close();
        }

        return testCases;
    }


}

